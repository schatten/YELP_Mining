
class DataSet:
    businesses = None
    reviews = None
    users = None
    checkins = None
    tips = None


class Business:
    business_id = None
    name = None
    neighborhoods = None  # list
    full_address = None
    city = None
    state = None
    latitude = None
    longitude = None
    stars = None
    review_count = None
    categories = None  # list
    is_open = None
    hours = None # dictionary
    attributes = None # dictionary


class Review:
    business_id = None
    user_id = None
    stars = None
    text = None
    date = None  # '2012-03-14', %Y-%m-%d in strptime notation
    useful_votes = None
    funny_votes = None
    cool_votes = None


class User:
    user_id = None
    name = None
    review_count = None
    average_stars = None
    useful_votes = None
    funny_votes = None
    cool_votes = None
    friends = None # list
    elite = None  # list
    yelping_since = None # date, formatted like '2012-03'
    compliments = None # dictionary
    fans = None


class Checkin:
    business_id = None
    checkin_info = None # dictionary


class Tip:
    business_id = None
    text = None
    user_id = None
    date = None  # '2012-03-14', %Y-%m-%d in strptime notation
    likes = None


class Pattern:
    features = None
    label = None


class Features:
    business_features = None
    user_features = None
    review_features = None
