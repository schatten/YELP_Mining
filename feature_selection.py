import sys, ast
import pandas as pd
import sklearn.preprocessing as prepro
import numpy as np
from sklearn.feature_selection import SelectKBest, f_regression


def feature_analysis(input_file, columns_to_ignore, top_features):
    print("Analysing features from dataset: " + input_file)

    print("Reading dataset...")
    data = pd.read_csv(input_file)

    # TODO handle categorical features via OneHotEncoder (so far each category is represented as an integer or binary)

    # remove the columns to be ignored (representing ids)
    columns_to_ignore_list = ast.literal_eval(columns_to_ignore)
    data.drop(data.columns[columns_to_ignore_list], axis=1, inplace=True)

    # scale the dataset
    print("Scaling dataset...")
    data_scaled = prepro.robust_scale(data)
    data_scaled = prepro.minmax_scale(data_scaled)

    # split features and labels
    features = data_scaled[:, 0:-1]
    label = data_scaled[:, -1]

    # analyse features (F-measure ans p-value)
    print("Analysing features (f-measure and p-value)...")
    f_measures, p_values = f_regression(features, label)
    f_measures /= np.max(f_measures)

    # show results in command line
    print("Results of feature analysis:")
    print("FEATURE_NAME\tF_MEASURE\tP_VALUE")
    for i in range(0, len(data.columns.values) - 1):
        feature_name = data.columns[i]
        feature_f_score = f_measures[i]
        feature_p_value = p_values[i]
        print(feature_name + "\t" + str(feature_f_score) + "\t" + str(feature_p_value))

    # select the top-k features
    print("Selecting top-" + str(top_features) + " features...")
    selector = SelectKBest(f_regression, k=ast.literal_eval(top_features))
    selector.fit(features, label)
    features_reduced = selector.transform(features)
    print("Done.")

    return features_reduced, label


if __name__ == "__main__":

    feature_analysis(sys.argv[1], sys.argv[2], sys.argv[3])
