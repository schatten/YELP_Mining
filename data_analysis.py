import os
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import read_dataset as rd
import sys, ast

plt.style.use('ggplot')

MAX_UNIQUE_VALUES = 200
SCATTER_MATRIX = False


def analyse_attribute(dataset, attribute, attributes_folder):
    print("Analysing attribute: " + attribute.name)

    # compute and store the boxplot (only for numerical attributes)
    if (dataset[attribute.name].dtype == "float64") or (dataset[attribute.name].dtype == "int64"):
        plot = dataset.boxplot(attribute.name)
        plt.savefig(attributes_folder + "/" + attribute.name + "_box_plot.png")
        plt.clf()

    # compute and store the distribution (histogram for numeric attributes, value counts for categorical attributes)
    if (dataset[attribute.name].dtype == "float64") or (dataset[attribute.name].dtype == "int64"):
        dataset[attribute.name].fillna(-1, inplace=True)

        # define the range of the histogram based on the mean (to reduce the effect of the outliers)
        mean_val = dataset[attribute.name].mean()
        std_val = dataset[attribute.name].std()
        low = max(dataset[attribute.name].min(), mean_val - 3*std_val)
        high = min(dataset[attribute.name].max(), mean_val + 3*std_val)

        plot = dataset.hist(attribute.name, bins=20, range=(low,high))
        plt.savefig(attributes_folder + "/" + attribute.name + "_histogram.png")
        plt.clf()
    else:
        dataset[attribute.name].fillna("missing", inplace=True)
        if dataset[attribute.name].nunique() <= MAX_UNIQUE_VALUES:
            plot = dataset[attribute.name].value_counts(sort=False).plot(kind='bar')
            plt.savefig(attributes_folder + "/" + attribute.name.replace("/","-") + "_histogram.png")
            plt.clf()


def analyse_dataset(input_file, output_folder, compute_scatter_matrix):
    print("Analysing dataset from csv: " + input_file)
    data = pd.read_csv(input_file)

    # create output folder to store the results of the analysis
    dataset_folder = output_folder + input_file.split("/")[-1].replace(".csv", "")
    if not os.path.exists(dataset_folder):
        os.makedirs(dataset_folder)

    # print and store aggregated statistics of each column (avg, std, min, max, mean, percentiles)
    fout = open(dataset_folder + "/description.txt", 'w+', encoding='utf-8', newline='')
    for column in data:
        description = data[column].describe()
        fout.write(column + "\n")
        fout.write(description.to_string())
        fout.write("\n\n")
    fout.close()

    # compute and store the scatter matrix of the dataset (only for the numerical attributes)
    if compute_scatter_matrix:
        print("computing scatter matrix...")
        scatter = pd.scatter_matrix(data._get_numeric_data(), alpha=0.2, figsize=(15, 15), diagonal='kde').all()
        plt.savefig(dataset_folder + "/scatter_matrix.png")
        plt.clf()

    # create the subfolder to contain the analysis of each column (attribute)
    attributes_folder = dataset_folder + "/attributes/"
    if not os.path.exists(attributes_folder):
        os.makedirs(attributes_folder)

    # analyse each attribute separately
    for column in data:
        analyse_attribute(data, data[column], attributes_folder)


if __name__ == "__main__":
    analyse_dataset(sys.argv[1], sys.argv[2], ast.literal_eval(sys.argv[3]))
    print("Done.")
