import pandas as pd
import numpy as np
from math import sqrt
import csv
import sys

MIN_HIST_LENGTH = 5
MIN = 1
MAX = 5
PATTERN = 60000
ALPHA = 0.002


def minmax(value):
    scaled_value = (value - MIN) / (MAX-MIN)
    return scaled_value


def matrix_factorization(R, P, Q, K,  beta, alpha=0.002, steps=200):
    Q = Q.T
    cumeij = 0
    counter = 0

    for step in range(0,steps):
        for i in range(0,len(R)):
            if R[i][2] >= 0:
                eij = R[i][2] - np.dot(P[R[i][0], :], Q[:, R[i][1]])
                cumeij += eij
                counter += 1
            for k in range(0,K):
                P[R[i][0]][k] = P[R[i][0]][k] + alpha * (2 * eij * Q[k][R[i][1]] - beta * P[R[i][0]][k])
                Q[k][R[i][1]] = Q[k][R[i][1]] + alpha * (2 * eij * P[R[i][0]][k] - beta * Q[k][R[i][1]])

        if step % 10 == 0:
            print("RMSE (train): " + str(sqrt(cumeij**2/counter)))

        cumeij = 0
        counter = 0

    return P, Q.T


def run_matrix_factorization(input_file, users_dictionary_file, businesses_dictionary_file):
    print("Running matrix factorization on dataset: " + input_file)
    data = pd.read_csv(input_file)

    data = data.ix[0:PATTERN, [36, 0, -1]]
    data = data.sort(data.columns[0])
    data = data.reset_index()
    data.drop(data.columns[0], axis=1, inplace=True)

    with open(users_dictionary_file, 'rt') as csv_file:
        reader = csv.reader(csv_file)
        UserCount_Dic = dict(reader)

    with open(businesses_dictionary_file, 'rt') as csv_file:
        reader = csv.reader(csv_file)
        BusinessCount_Dic = dict(reader)

    Users_MF = dict()
    i = 0
    for user in UserCount_Dic:
        if int(UserCount_Dic[user]) > MIN_HIST_LENGTH:
            Users_MF[user] = list([i, int(UserCount_Dic[user])])
            i += 1

    Businesses_MF = dict()
    i = 0
    for business in BusinessCount_Dic:
        Businesses_MF[business] = list([i, int(BusinessCount_Dic[business])])
        i += 1

    print("Filtering Cold User")
    data_Train = list()
    data_Test = list()

    Old_Name = None
    i = 0
    for index, datapoint in data.iterrows():
        if datapoint[data.columns[0]] in Users_MF:
           if list(Users_MF[datapoint[data.columns[0]]])[1] > MIN_HIST_LENGTH:

                if Old_Name != datapoint[data.columns[0]]:
                    Old_Name = datapoint[data.columns[0]]
                    i = 1
                else:
                    i += 1

                user_id = list(Users_MF[datapoint[data.columns[0]]])[0]
                business_id = list(Businesses_MF[datapoint[data.columns[1]]])[0]
                label = minmax(datapoint[data.columns[2]])

                if i < 0.66*list(Users_MF[datapoint[data.columns[0]]])[1]:
                    data_Train.append([user_id, business_id, label])
                else:
                    data_Test.append([user_id, business_id, label])

    print("Initializing MF")

    N = len(Users_MF)
    M = len(Businesses_MF)

    for lamb in range(1, 2):
        for K in range(5,6):
            P = np.random.rand(N, K)
            Q = np.random.rand(M, K)

            nP, nQ = matrix_factorization(data_Train, P, Q, K, lamb/100)

            nQ = nQ.T
            eij = 0

            for i in range(0, len(data_Test)):
                eij += data_Test[i][2] - np.dot(nP[data_Test[i][0], :], nQ[:, data_Test[i][1]])

            print("RMSE Test " + str(sqrt(eij**2/len(data_Test)))+" Regression hyperparameter "+str(lamb/100)+" Latent Feature "+str(K))


if __name__ == "__main__":
    run_matrix_factorization(sys.argv[1], sys.argv[2], sys.argv[3])